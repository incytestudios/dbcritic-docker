IMAGE_NAME=incytestudios/dbcritic
TAG=latest
SHELL=/bin/bash

.PHONY: image run
image:
	DOCKER_BUILDKIT=1 docker build -t $(IMAGE_NAME):$(TAG) --target final .

run:
	docker run --rm -it \
		-e PGHOST=0.0.0.0 \
		-e PGPORT=5432 \
		-e PGUSER=postgres \
		-e PGDATABASE=shutupwrite \
		-e PGPASSWORD=pw \
		--network=host \
		$(IMAGE_NAME):$(TAG)