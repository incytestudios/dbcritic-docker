FROM nixos/nix:2.3.12 as builder

RUN apk --no-cache add git && \
  git clone --depth 1 https://github.com/channable/dbcritic.git /dbcritic && \
  cd /dbcritic && \
  nix-build

FROM alpine:latest as final
COPY --from=builder /dbcritic/result/bin/dbcritic /bin/
COPY --from=builder /nix /nix
WORKDIR /root
RUN touch /root/.dbcriticrc
CMD ["dbcritic"]
