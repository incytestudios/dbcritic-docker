# dbcritic-docker

A dockerized version of https://github.com/channable/dbcritic

## Usage
1. `git clone https://gitlab.com/incytestudios/dbcritic-docker.git`
2. `cd dbcritic`
3. `make image`
4. set your postgres settings in the `Makefile`
5. `make run`
6. go fix your schema